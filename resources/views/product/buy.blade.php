<!DOCTYPE html>
@extends('navbar.navbar')
@section('content')
<html>
    <head>
        <style>
            h3 {
                color: #ef5285;
            }
            th,td {
                text-align: center;
            }
            .card {
                position: fixed;
                /* z-index: 2; */
                bottom: 5px;
                width: 73%;
                margin-top: 50px;
                background-color: #feee7d;
            }
            .list {
                margin-bottom: 150px;
            }
            .img {
                width: 50px;
                border: 1px solid grey;
            }
            .black span {
                font-size: 16px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            {{-- black --}}
            <div class="black mb-5" align="left">
                <a href="/" type="button" class="btn btn-secondary">
                    <i class="material-icons" style="font-size:16px;color:white;">arrow_back</i>
                    <span>ย้อนกลับ</span>
                </a>
            </div>

            <h3>รถเข็น</h3>
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
            </div>
            <form action="" method="post">
            <table class="table">
                <thead>
                  <tr>
                    <th scope="col">No.</th>
                    <th scope="col" style="width:300px;">สินค้า</th>
                    <th scope="col">ราคาต่อชิ้น</th>
                    <th scope="col">จำนวน</th>
                    <th scope="col">ราคารวม</th>
                    <th scope="col">แอคชั่น</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($carts as $index => $cart)
                    @csrf
                  <tr>
                    <th scope="row">{{$index + 1}}</th>
                    <td class="text-left">
                        <p class="card-title">{{$cart->name}}</p>
                    </td>
                    <td>{{$cart->price}}</td>
                    <td>{{$cart->amount}}</td>
                    <td>{{$cart->total}}</td>
                    <td>
                        <a href="{{route('product.edit.page', $cart ->id)}}" type="button" class="btn btn-warning">
                            <i class="material-icons" style="font-size:20px;color:black">launch</i>
                        </a>
                        <a href="{{ route('cart.delete',$cart->id)}}" type="button" class="btn btn-danger">
                            <i class="material-icons" style="font-size:20px;color:black">delete</i>
                        </a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
            </table>


            <div class="btn-buy mb-10" align="center" style="margin-top: 50px;margin-bottom: 70px;">
                <a href="{{route('invoice')}}" type="button" class="btn btn-danger" style="width:100px;">
                    สั่งสินค้า
                </a>
            </div>
            </form>
        </div>
    </body>
</html>
@endsection
