<!DOCTYPE html>
@extends('navbar.navbar')
@section('content')
<html>
    <head>
        <style>
            .card {
                border: 1px solid grey;
                border-radius: 5px;
            }
            .header {
                background-color: #1a73e8; /*#a5dff9 #1a73e8*/
                padding: 20px 50px;
                color: white;
            }
            .logo {
                font-family: Pacifico;
                color: aliceblue;
                font-size: 30px;
            }
            .contain {
                padding: 20px 50px;
            }
            .contain p {
                color: grey;
            }
             th,td {
                text-align: right !important;
            }
            .list-invoice {
                margin-top: 120px;
            }
            hr {
                border-top: 3px solid #a5dff9;
            }
            .black span {
                font-size: 16px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            {{-- black --}}
            <div class="black mb-5" align="left">
                <a href="{{route('buy')}}" type="button" class="btn btn-secondary">
                    <i class="material-icons" style="font-size:16px;color:white;">arrow_back</i>
                    <span>ย้อนกลับ</span>
                </a>
            </div>
            <div class="card">
                <div class="header">
                   <div class="row">
                       <div class="col-md-9">
                        <p class="nav-link logo">Gwangly Shop </p>
                       </div>
                       <div class="col-md-3" align="right">
                            <p style="font-size:28px;"> INVOICE </p>
                            <span style="padding:8px;">Username</span>
                            <p style="padding:8px;"> {{ Auth::user()->name }}</p>
                       </div>
                   </div>
                </div>
                <div class="contain">
                    <div class="float-left">
                        <p>Invoice Number :  <span style="font-size:15px; color:black;">0123456789</span></p>
                        <p>Date Of Issue : <span style="font-size:15px; color:black;">23-12-2019</span></p>
                    </div>
                    <div class="float-right">
                        <p>Invoice Total</p>
                        <p class="text-right" style="font-size:30px; color:black;">฿ 290</p>
                    </div>

                    <div class="list-invoice">
                        <hr/>
                        <table class="table">
                            <thead>

                                <tr class="mt-5">
                                    <th scope="col" style=" text-align:left !important;">สินค้า</th>
                                    <th scope="col">ราคาต่อชิ้น</th>
                                    <th scope="col">จำนวน</th>
                                    <th scope="col">ราคารวม</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($carts as $index => $cart)
                                @csrf
                                <tr>
                                    <td class="text-left">{{$cart->name}}</td>
                                    <td>{{$cart->price}}</td>
                                    <td>{{$cart->amount}}</td>
                                    <td>{{$cart->total}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
    </body>
</html>
@endsection
