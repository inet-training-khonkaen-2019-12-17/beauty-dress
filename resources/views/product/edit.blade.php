@extends('navbar.navbar')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" style="background: #60c5ba;">Edit Num #{{ $cart->name }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form action="{{ route('product.edit') }}" method="post">
                      @csrf
                      <input type="hidden" value="" name="id">
                        <div class="form-group">
                          <label for="name">Name</label>
                          <input type="text" class="form-control" name="name" id="name" aria-describedby="name" value="{{ $cart->name }}" readonly="readonly" required>
                        </div>
                        <div class="form-group">
                            <label for="author">Price</label>
                            <input type="number" class="form-control" name="price" id="" aria-describedby="author" value="{{ $cart->price }}" required>
                        </div>
                        <div class="form-group">
                              <label for="price">Detail</label>
                              <input type="text" class="form-control" name="amount" id="price" aria-describedby="price" value="{{ $cart->amount }}" readonly="readonly" required>
                        </div>
                        <div class="form-group">
                              <label for="describe">Describe</label>
                              <input type="text" class="form-control" name="total" id="describe" aria-describedby="describe" value="{{ $cart->total }}" readonly="readonly" required>
                        </div>
                        {{--  <div class="form-group">
                            <label for="describe">Type</label>
                            <select class="form-control" name="type">
                                <option
                                @if($book->type === 'นิตสาร')
                                  {{ "selected" }}
                                @endif
                                >นิตยสาร</option>
                                <option
                                @if($book->type === 'บันเทิง')
                                  {{ "selected" }}
                                @endif
                                >บันเทิง</option>
                                <option
                                @if($book->type === 'สารคดี')
                                  {{ "selected" }}
                                @endif
                                >สารคดี</option>
                            </select>
                        </div>  --}}
                        <a href="{{ route('buy') }}" class="btn btn-danger">Back</a>
                        <button type="submit" class="btn btn-success">Save</button>
                      </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
