<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <!-- font -->
  <link href="https://fonts.googleapis.com/css?family=Kanit&display=swap" rel="stylesheet">
  <!-- Icon -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

  <title>Document</title>
  <style>
    @import url('https://fonts.googleapis.com/css?family=Kanit&display=swap');
    @import url('https://fonts.googleapis.com/css?family=Pacifico&display=swap');
    @import url('https://fonts.googleapis.com/css?family=Nunito&display=swap');
    @import url('https://fonts.googleapis.com/css?family=Sarabun&display=swap');
    body {
      font-family: kanit;
    }
    .nav-color {
      background: #60c5ba;
    }
    .logo {
      font-family: Pacifico;
      color: aliceblue;
      font-size: 30px;
    }
    .font-color {
      color: aliceblue;
    }
    .pill-color{
      background: #ef5285;
    }
    .footer {
        bottom: 0;
        width: 100%;
        height: 80px;
    }
    .btn-add {
      background: #feee7d;
    }
    .head {
      background: #60c5ba;
    }
    .table th,td {
      text-align: center;   
      font-size: 14px;
    }
    a, a:hover {
      color: white;
    }
    .footer {
      position: fixed;
      left: 0;
      bottom: 0;
      width: 100%;
      background-color: #60c5ba;
      color: white;
      text-align: center;
    }
    .backgroundcolor{
            background-color: #d6ecfa;
        }
    .footer p {
        padding-top: 5px;
    }
  </style>
</head>
<body class="backgroundcolor">
    <nav class="navbar navbar-expand-lg nav-color">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
          <a class="nav-link logo" href="/">Gwangly Shop</a>
        </div>
          <ul class="nav justify-content-end">
            @guest
              <li class="nav-item active">
                <a class="nav-link font-color" href="{{ route('login') }}">{{ __('Login') }}</a>
              </li>
              @if (Route::has('register'))
                <li class="nav-item active">
                    <a class="nav-link font-color" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
              @endif
              @else
                <li class="nav-item dropdown active">
                  <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }}
                  </a>
                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                      {{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                  </div>
                </li>
            @endguest
          </ul>
        </nav>
    
    <main class="py-4">
        @yield('content')
    </main>
     {{-- Footer --}}
    <div class="footer">
        <p class="copyright-text">Copyright &copy; 2017 All Rights Reserved by
            <a href="#" style="font-family: Pacifico; ">Gwangly</a>.
        </p>
    </div>
</body>
</html>
