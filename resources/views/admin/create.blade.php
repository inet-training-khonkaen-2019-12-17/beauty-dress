@extends('navbar.navbar')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" style="background: #60c5ba;">Create New Book</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form action="{{ route('admin.create') }}" method="post">
                      @csrf
                      <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" id="name" aria-describedby="name" required>
                      </div>
                      <div class="form-group">
                          <label for="author">Price</label>
                          <input type="number" class="form-control" name="price" id="author" aria-describedby="author" required>
                      </div>
                      <div class="form-group">
                        <label for="name">Detail</label>
                        <input type="text" class="form-control" name="detail" id="name" aria-describedby="name" required>
                      </div>
                      <div class="form-group">
                            <label for="image_pro">Image</label>
                            <input type="file" class="form-control" name="image_pro" id="image_pro" aria-describedby="image_pro" required>
                      </div>

                        {{--  <div class="form-group">
                            <label for="describe">Type</label>
                            <select class="form-control" name="type">
                                <option>นิตสาร</option>
                                <option>บันเทิง</option>
                                <option>สารคดี</option>
                            </select>
                        </div>  --}}
                        <a href="{{ route('admin.page') }}" class="btn btn-danger">Back</a>
                        <button type="submit" class="btn btn-success">Save</button>
                      </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
