@extends('navbar.navbar')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" style="background: #60c5ba;">Edit Book #{{ $product->name }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form action="{{ route('admin.edit') }}" method="post">
                      @csrf
                      <input type="hidden" value="{{ $product->id }}" name="id">
                        <div class="form-group">
                          <label for="name">Name</label>
                          <input type="text" class="form-control" name="name" id="name" aria-describedby="name" value="{{ $product->name }}" required>
                        </div>
                        <div class="form-group">
                            <label for="author">Price</label>
                            <input type="number" class="form-control" name="author" id="author" aria-describedby="author" value="{{ $product->price }}" required>
                        </div>
                        <div class="form-group">
                              <label for="price">Detail</label>
                              <input type="text" class="form-control" name="price" id="price" aria-describedby="price" value="{{ $product->detail }}" required>
                        </div>
                        <div class="form-group">
                              <label for="describe">Describe</label>
                              <input type="text" class="form-control" name="describe" id="describe" aria-describedby="describe" value="{{ $product->image_pro }}" required>
                        </div>
                        {{--  <div class="form-group">
                            <label for="describe">Type</label>
                            <select class="form-control" name="type">
                                <option
                                @if($book->type === 'นิตสาร')
                                  {{ "selected" }}
                                @endif
                                >นิตยสาร</option>
                                <option
                                @if($book->type === 'บันเทิง')
                                  {{ "selected" }}
                                @endif
                                >บันเทิง</option>
                                <option
                                @if($book->type === 'สารคดี')
                                  {{ "selected" }}
                                @endif
                                >สารคดี</option>
                            </select>
                        </div>  --}}
                        <a href="{{ route('admin.page') }}" class="btn btn-danger">Back</a>
                        <button type="submit" class="btn btn-success">Save</button>
                      </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
