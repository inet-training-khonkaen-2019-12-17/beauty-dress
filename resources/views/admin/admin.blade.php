@extends('navbar.navbar')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
             {{-- <div class="card">
                <div class="card-header head">Dashboard</div>  --}}

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div align="right" style="margin-bottom: 30px">
                      <a href="{{ route('admin.create.page') }}" class="btn btn-add">
                          + Product
                      </a>
                    </div>
                    <table class="table table-striped table-hover">
                      <thead>
                        <tr>
                          <th scope="col" >#</th>
                          <th scope="col">รหัสสินค้า</th>
                          <th scope="col" style="max-width:100px;">ชื่อสินค้า</th>
                          <th scope="col">ราคา</th>
                          <th scope="col">รายละเอียด</th>
                          <th scope="col">รูปสินค้า</th>
                          <th scope="col">edit</th>
                          <th scope="col">delete</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($products as $index => $product)
                        <tr>
                            <td scope="row">{{ $index + 1 }}</td>
                            <td>{{ $product->id }}</td>
                            <td>{{ $product->name }}</td>
                            <td>{{ $product->price }}</td>
                            <td>{{ $product->detail }}</td>
                            <td>{{ $product->image_pro }}</td>
                            <td><a href="{{ route('admin.edit.page', $product->id) }}" type="button" class="btn btn-warning"><i class="fas fa-edit"></i></a></td>
                            <td><a href="{{ route('admin.delete', $product->id) }}" type="button" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a></td>
                      </tr>
                        @endforeach
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    {{--  </div>  --}}
</div>
<script src="https://kit.fontawesome.com/a076d05399.js"></script>
@endsection
