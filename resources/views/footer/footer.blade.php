<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Kanit&display=swap" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  <title>Document</title>
  <style>
    @import url('https://fonts.googleapis.com/css?family=Kanit&display=swap');
    @import url('https://fonts.googleapis.com/css?family=Pacifico&display=swap');
    body {
      font-family: kanit;
    }
    .footer-color {
      background: #60c5ba;
    }
    .logo {
      font-family: Pacifico;
      color: aliceblue;
      font-size: 30px;
    }
    .font-color {
      color: aliceblue;
    }
    .pill-color{
      background: #ef5285;
    }
  </style>
</head>
<body>
    <footer class="footer navbar-expand-lg nav-color">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
          <a class="nav-link logo" href="/">Gwangly Shop</a>
        </div>
          <ul class="nav justify-content-end">
            <li class="nav-item active">
              <a class="nav-link font-color" href="{{ route('login') }}">LOGIN <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
              <a class="nav-link font-color" href="{{ route('register') }}">REGISTER <span class="sr-only">(current)</span></a>
            </li>
          </ul>
    </footer>
<main class="py-4">
  @yield('footer')
</main>
</body>
</html>