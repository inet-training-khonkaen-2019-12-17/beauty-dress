<!DOCTYPE html>
@extends('navbar.navbar')
@section('content')
<html>
    <head>
        <script>
            $(document).ready(function(){

                $('.qtyplus').click(function(e){
                    e.preventDefault();
                    var fieldName = $(this).prev();
                    var currentVal = parseInt(fieldName.val());
                    if (!isNaN(currentVal )&& currentVal < 5) {
                        fieldName.val(currentVal + 1);
                        $data = $('.data').val();
                        $('[name="totaltext"').text($data * (currentVal + 1));
                        $('.total').val($data * (currentVal + 1));
                    } else {
                        fieldName.val(5);
                    }
                });
                $(".qtyminus").click(function(e) {
                    e.preventDefault();
                    var fieldName = $(this).next();
                    var currentVal = parseInt(fieldName.val());
                    if (!isNaN(currentVal) && currentVal > 0) {
                        fieldName.val(currentVal - 1);
                        $data = $('.data').val();
                        $total = $('.total').val();
                        $('[name="totaltext"').text($total - $data);
                        $('.total').val($total - $data);
                    } else {
                        fieldName.val(0);
                    }
                });
            });
        </script>

        <style>
            .container {
                margin-bottom: 50px;
            }
            .img-title {
                width: 50px;
                margin: -15px 0px 20px 10px;
            }
            .row {
                margin-top: 20px;
            }
            .card-img {
                width: 175px;
                height: 200px;
                margin: 10px;
            }
            }
            .contain {
                text-align: center;
            }
            .card {
                cursor: pointer;
            }
            .info {
                float: left;
                text-align: center;
            }
        </style>
    </head>

    <body>
        <div class="container">
            <br>
           <div class="row" align="center">
                <p class="text-center" style="font-size:20px;">แคตตาล็อค</p>
                <img class="img-title" src="./imgProduct/1.png" alt="title">
           </div>
            <div>
            @foreach($products as $index => $product)
                <div class="col col-lg-3">
                    <div class="row">
                        <div class="card" style="width: 200px;" data-toggle="modal" data-target="#modal-{{$product->id}}">
                            <img class="card-img" src="{{$product->image_pro1}}" alt="Card image">
                            <div class="card-body">
                                <h5 class="card-title">{{$product-> name}}</h5>
                                <p class="card-text text-right">{{$product-> price}}&nbsp;THB</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade align-center" id="modal-{{$product->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">


                        <div class="modal-header">
                            <h4 class="modal-title" id="name">{{$product-> name}}</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="{{ route('cart.create')}}" method="post">
                            @csrf
                            {{-- <input type="hidden" value="{{ $product->id }}" name="id"> --}}
                            <input type="hidden" value="{{ $product->name }}" name="name" id="name">
                            <input type="hidden" value="{{ $product->price }}" name="price" id="price">
                            {{-- <input type="hidden" value="{{ $product->image_pro1 }}" name="image_pro1" id="image_pro1"> --}}

                        <div class="modal-body">
                            <img class="card-img" src="{{$product->image_pro1}}" alt="Card image">
                            <img class="card-img" src="{{$product->image_pro2}}" alt="Card image">
                            <div class="text-center">
                                <h5 >{{$product->detailname}}</h5><br>
                            <p class="text-left">{{$product->detail}}</p>
                            </div>
                            <div class="text-right">
                            <input type="hidden" class="data" value="{{$product->price}}"/>
                            <p> ราคา <label for="totaltext" name="totaltext">{{$product->price}}</label>  บาท</p>
                            <input type="hidden" name="total" id="total" class="total" value="{{$product->price}}"/>
                                <input type="button" value="-" class="qtyminus" field="quantity" />
                                <input type="text" name="amount" id="amount" value="1" style="text-align: center; width: 50px;"/>
                                <input type="button" value="+" class="qtyplus" field="quantity" />

                                {{-- <input type="text" value="{{ $product->price*$amount }}" name="total" id="total"> --}}
                            </div>
                        </div>
                        <div class="modal-footer">
                            <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
                            <button type="submit" class="btn" style="background-color: #feee7d;">เก็บใส่ตะกร้า</button>
                        </div>
                        </form>
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
    </body>
</html>
@endsection
