<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['user']], function () {
Route::get('/logout','User\UserController@logout')->name('logout');
Route::post('/logout','User\UserController@logout')->name('logout');
Route::get('/profile', 'User\UserController@profile')->name('profile');
Route::post('/onechat/store' , 'User\UserController@storeOneChat')->name('onechat.store');

Route::get('/admin','ProductController@adminPage')->name('admin.page');
Route::get('/admin/create','User\UserController@createPage')->name('create.page');
Route::post('/create','User\UserController@create')->name('create');

Route::get('/admin/create','ProductController@createPage')->name('admin.create.page');
Route::post('/admin/create','ProductController@create')->name('admin.create');

Route::get('/admin/edit/{id}','ProductController@editPage')->name('admin.edit.page');
Route::post('/admin/edit/','ProductController@edit')->name('admin.edit');

Route::get('/admin/delete/{id}','ProductController@delete')->name('admin.delete');
Route::post('/', 'CartController@create') ->name('cart.create');
Route::get('/cart/delete/{id}', 'CartController@delete')->name('cart.delete');

Route::get('/product/buy','CartController@cart')->name('buy');

Route::get('/product/edit/{id}','ProductController@editNumPage')->name('product.edit.page');
Route::post('/product/edit/','ProductController@editNum')->name('product.edit');


Route::get('/invoice','ProductController@invoice')->name('invoice');
});

Route::get('/', 'ProductController@index');

Route::get('/register','User\UserController@registerPage')->name('register.page');
Route::post('/register','User\UserController@register')->name('register');

Route::get('/login','User\UserController@loginPage')->name('login.page');
Route::post('/login','User\UserController@login')->name('login');


Route::post('/logout','User\UserController@logout')->name('logout');


// Route::post('/', 'CartController@create') ->name('cart.create');
// Route::get('/cart/delete/{id}', 'CartController@delete')->name('cart.delete');

// Route::get('/buy','CartController@cart')->name('buy');

// Route::get('/product/edit/{id}','ProductController@editNumPage')->name('product.edit.page');
// Route::post('/product/edit/','ProductController@editNum')->name('product.edit');


// Route::get('/invoice','ProductController@invoice')->name('invoice');
