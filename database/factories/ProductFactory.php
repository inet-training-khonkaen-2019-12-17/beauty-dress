<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
            'name' => 'dress',
            'Price' => '290',
            'detailname' => 'test',
            'detail' => 'สี: กรม/ขาว/เหลือง/ฟ้า',
            'image_pro1' => 'https://www.img.in.th/images/a254518ae395a3eae26651c2e0bf5df6.jpg',
            'image_pro2' => 'https://www.img.in.th/images/140220317c736c9bbaa22474cdd7b619.jpg',
        ];
    });
