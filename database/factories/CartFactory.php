<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Cart;
use Faker\Generator as Faker;

$factory->define(Cart::class, function (Faker $faker) {
    return [
        'name' => 'dress',
        'Price' => '290',
        // 'image_pro1' => 'https://www.img.in.th/images/a254518ae395a3eae26651c2e0bf5df6.jpg',
        'amount'=>'1',
        'total'=>'$Price*$amount' ,
    ];
});
