<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        factory(App\Product::class,1)->create([
            'name' =>'เดรสคอปก',
             'detailname' =>'เดรสฮานาโกะคอปกแต่งชีฟองคลุมรอบพร้อมแต่งกระดุมผ้ารุ่นฮิตที่ใครๆก็ถามหาจ้า',
            'detail'=>'สี:กรม/เทาฟ้า/แดง/ชมพู/โกโก้ อกได้ถึง 38” เอวได้ถึง 32” สะโพกได้ถึง 40” ความยาว 35”',
            'image_pro1' => 'https://www.img.in.th/images/a254518ae395a3eae26651c2e0bf5df6.jpg',
            'image_pro2' => 'https://www.img.in.th/images/140220317c736c9bbaa22474cdd7b619.jpg',


        ]);
        factory(App\Product::class,1)->create([
            'name' =>'เดรสคลุมไหล่',
            'detailname' =>'เดรสฮานาโกะตัวฮิตมาแล้วจ้าาา ชีฟองคลุมไหล่หรูหราสุดๆ',
            'detail'=>'สี: กรม/แดง/ครีม/ชมพู/โกโก้ อกได้ถึง 38” เอวได้ถึง 32” สะโพกได้ถึง 40” ความยาว 35”',
            'image_pro1' => 'https://www.img.in.th/images/767fe170eda4c6e88d1094319d5c77a1.jpg',
            'image_pro2' => 'https://www.img.in.th/images/937db16502e75607458bf1e89a60730d.jpg',

        ]);
        //3
        factory(App\Product::class,1)->create([
            'name' =>'เดรสฟรุ้งฟริ้ง',
            'detailname' =>'เดรสฮานาโกะแต่งผ้าชีฟองอัดพลีดช่วงอกและแขน ฟรุ้งฟริ้งน่ารักสุดๆเลยค่า',
            'detail'=>'สี: กรม/แดง/ครีม/ชมพู/โกโก้ อกได้ถึง 38” เอวได้ถึง 32” สะโพกได้ถึง 40” ความยาว 35”',
            'image_pro1' => 'https://www.img.in.th/images/2a2c8b4f3a680f5ece3e1e112cfe144c.jpg',
            'image_pro2' => 'https://www.img.in.th/images/4404441fa262a05ddda1011a8b65bb7e.jpg',
        //4
        ]);
        factory(App\Product::class,1)->create([
            'name' =>'เดรสแต่งกุ้น',
            'detailname' =>'เดรสฮานาโกะดีไซน์ใหม่แต่งคลุมด้วยผ้าชีฟองแต่งกุ้นสีคัดกันโดดเด่นสุดจ้า งานเป้ะสุดๆเลยจ้าา',
            'detail'=>'สี: กรม/แดง/เทาอ่อน/เทาฟ้า/อิฐ อกได้ถึง 38" เอวได้ถึง 32" สะโพกได้ถึง 40" ความยาว 35"',
            'image_pro1' => 'https://www.img.in.th/images/ff0b435d013fa3b913fd24128c7455a7.jpg',
            'image_pro2' => 'https://www.img.in.th/images/b62d2a141459f8b46160b0dd14017e26.jpg',
        ]);
        // 5
        factory(App\Product::class,1)->create([
            'name' =>'เดรสทรีโทน',
            'detailname' =>'เดรสฮานาโกะแต่งสีทรีโทนสลับ ทรงปล่อยๆ รุ่นนี้ฮิตกันสุดๆเลยจ้า',
            'detail'=>'สี: กรม/อิฐ/เทาฟ้า/เทาอ่อน/โกโก้ อกได้ถึง 38" เอวได้ถึง 32" สะโพกได้ถึง 40" ความยาว 35”',
            'image_pro1' => 'https://www.img.in.th/images/72c916966cec4f7ac50d9328e1a0d399.jpg',
            'image_pro2' => 'https://www.img.in.th/images/f8101d19a2cb1022ed78e600f229fe19.jpg',
        ]);
        //6
        factory(App\Product::class,1)->create([
            'name' =>'เดรสชีฟองผ่า',
            'detailname' =>'เดรสฮานาโกะแต่งแขนชีฟองผ่า',
            'detail'=>'สี: กรม/ชมพู/โกโก้/เทาอ่อน/แดง/เทาฟ้า อกได้ถึง 38" เอวได้ถึง 32" สะโพกได้ถึง 40" ความยาว 35"',
            'image_pro1' => 'https://www.img.in.th/images/8858ba0838fbbed593a319fd1541f7d9.jpg',
            'image_pro2' => 'https://www.img.in.th/images/c79e5a99fc40c1d8ffe4ee0bd157a656.jpg',
        ]);
        //7
        factory(App\Product::class,1)->create([
            'name' =>'เดรสผ้ามุ้ง',
            'detailname' =>'เดรสผ้าฮานาโกะแต่งคลุมด้วยผ้ามุ้งปักดอก ด้านบนแต่งเป็นสายเดี่ยวด้านใน กระโปรงทรงเอบานนิดๆ',
            'detail'=>'สี:ม่วง/เทาฟ้า/แดง/ดำ อกได้ถึง 38” เอวได้ถึง 32” สะโพกได้ถึง 40” ความยาว 35”',
            'image_pro1' => 'https://www.img.in.th/images/a0bdbd48f9c67c8793d247fabc0cdb59.jpg',
            'image_pro2' => 'https://www.img.in.th/images/238fe525dbe80f2f4509c4c76611d087.jpg',
        ]);
        //8
        factory(App\Product::class,1)->create([
            'name' =>'มินิเดรส',
            'detailname' =>'มินิเดรสผ้าชีฟองทรงเกาหลีสุดๆจับจีบด้านล่าง มาพร้อมเชือกผูกเอว ใส่งานใส่สบายสุดๆ',
            'detail'=>'สี: กรม/ขาว/เหลือง/ฟ้า อกได้ถึง 38” เอวได้ถึง32” สะโพกได้ถึง 40” ความยาว 35”',
            'image_pro1' => 'https://www.img.in.th/images/88d37adf46af5cfdf83eb8f67420a387.jpg',
            'image_pro2' => 'https://www.img.in.th/images/ae06dc947e9197c581280991d58fb541.jpg',
        ]);
         //9
         factory(App\Product::class,1)->create([
            'name' =>'เดรสลายดอก',
            'detailname' =>'เดรสชีฟองลายดอกเนื้อสวยแต่งเหมือนเสื้อกับกระโปรง แขนระบาย มีเชือกผูกโบว์ น่ารักลุคเกาหลีสุดๆ มีซับในอย่างดีใส่แล้วอยู่ทรง',
            'detail'=>'สี:เหลือง/ฟ้า/ขาว/ชมพู อกได้ถึง 38” เอวได้ถึง 32” สะโพกได้ถึง 40” ความยาว 35”',
            'image_pro1' => 'https://www.img.in.th/images/862bffe0ebe1c315f5d098e10b0d941b.jpg',
            'image_pro2' => 'https://www.img.in.th/images/893a8d36cfe84f05abdde644eefa4069.jpg',
        ]);
         //10
         factory(App\Product::class,1)->create([
            'name' =>'เดรสลายตาราง',
            'detailname' =>'เดรสฮานาโกะแต่งระบายทูโทนด้านข้างดีไซน์ใหม่ ทรงคอวีใส่สบายๆ แต่งสีขับผิวสุดๆ ระบายสวยงามด้านหน้า ใช้ได้ทุกงานจริงๆจ้า',
            'detail'=>'สี:กรมแต่งมัสตาร์ด ม่วงแต่งนู้ด เขียวเป็ดแต่งนู้ด น้ำตาลแต่งดำ ชมพูแต่งแดง อกได้ถึง 38” เอวได้ถึง 32” สะโพกได้ถึง 40” ความยาว 35””',
            'image_pro1' => 'https://www.img.in.th/images/fd447590d30e2648208f9ae1b85d4a12.jpg',
            'image_pro2' => 'https://www.img.in.th/images/9e04346b51533645e94c767b67dd1ad8.jpg',
        ]);
        factory(App\Cart::class,1)->create([
            'name' => 'เดรสคอปก',
            'Price' => '290',
            // 'image_pro1' => 'https://www.img.in.th/images/a254518ae395a3eae26651c2e0bf5df6.jpg',
            'amount'=>'1',
            'total'=>'290'
        ]);
        //2
        factory(App\Cart::class,1)->create([
        'name' =>'เดรสคลุมไหล่',
        'Price' => '290',
        // 'image_pro1' => 'https://www.img.in.th/images/767fe170eda4c6e88d1094319d5c77a1.jpg',
        'amount'=>'1',
        'total'=>'290'
        ]);
        //3
        factory(App\Cart::class,1)->create([
        'name' =>'เดรสฟรุ้งฟริ้ง',
        'Price' => '290',
        // 'image_pro1' => 'https://www.img.in.th/images/2a2c8b4f3a680f5ece3e1e112cfe144c.jpg',
        'amount'=>'2',
        'total'=>'580'
     ]);
     //4
     factory(App\Cart::class,1)->create([
        'name' =>'เดรสแต่งกุ้น',
        'Price' => '290',
        // 'image_pro1' => 'https://www.img.in.th/images/ff0b435d013fa3b913fd24128c7455a7.jpg',
        'amount'=>'2',
        'total'=>'580'
     ]);
      //5
      factory(App\Cart::class,1)->create([
        'name' =>'เดรสทรีโทน',
        'Price' => '290',
        // 'image_pro1' => 'https://www.img.in.th/images/72c916966cec4f7ac50d9328e1a0d399.jpg',
        'amount'=>'2',
        'total'=>'580'
     ]);
     factory(App\Cart::class,1)->create([
        'name' => 'เดรสคอปก',
        'Price' => '290',
        // 'image_pro1' => 'https://www.img.in.th/images/a254518ae395a3eae26651c2e0bf5df6.jpg',
        'amount'=>'1',
        'total'=>'290'
    ]);
    //7
    factory(App\Cart::class,1)->create([
    'name' =>'เดรสคลุมไหล่',
    'Price' => '290',
    // 'image_pro1' => 'https://www.img.in.th/images/767fe170eda4c6e88d1094319d5c77a1.jpg',
    'amount'=>'1',
    'total'=>'290'
    ]);
    //8
    factory(App\Cart::class,1)->create([
    'name' =>'เดรสฟรุ้งฟริ้ง',
    'Price' => '290',
    // 'image_pro1' => 'https://www.img.in.th/images/2a2c8b4f3a680f5ece3e1e112cfe144c.jpg',
    'amount'=>'2',
    'total'=>'580'
 ]);
 //9
 factory(App\Cart::class,1)->create([
    'name' =>'เดรสแต่งกุ้น',
    'Price' => '290',
    // 'image_pro1' => 'https://www.img.in.th/images/ff0b435d013fa3b913fd24128c7455a7.jpg',
    'amount'=>'2',
    'total'=>'580'
 ]);
  //10
  factory(App\Cart::class,1)->create([
    'name' =>'เดรสทรีโทน',
    'Price' => '290',
    // 'image_pro1' => 'https://www.img.in.th/images/72c916966cec4f7ac50d9328e1a0d399.jpg',
    'amount'=>'2',
    'total'=>'580'
 ]);

    }
}
