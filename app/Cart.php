<?php

namespace App;

use App\Cart;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    //
    protected $fillable = ['id', 'name', 'price','amount','total'];
}
