<?php

namespace App\Http\Controllers;

use App\Cart;
use Illuminate\Http\Request;

class CartController extends Controller
{
    //
    public function cart()
    {
       $carts = Cart::all();
       return view('product.buy',['carts'=>$carts]);
    }

    public function createPage()
    {
        return view('cart.create');
    }

    public function create(Request $request)
    {
        // dd($request->all());
        // dump($request->name);
        // dd($request->all());
        $cart = new Cart();
        $cart->name = $request->name;
        $cart->price = $request->price;
        $cart->amount = $request->amount;
        $cart->total = $request->total;

        // $cart->describe = $request->describe;
        // $cart->type = $request->type;
        // dd($cart->save());

        if(!$cart->save()){
            return redirect()->route('/');
        }
        return redirect()->route('buy');
    }

    // public function editPage($id)
    // {
    //     // dd($id);
    //     $cart = Cart::find($id);
    //     return view('cart.edit', compact('cart'));
    // }

    // public function edit(Request $request)
    // {
    //     // dd($request->all());
    //     $book = Book::find($request->id);
    //     // dump($book);
    //     $book->name = $request->name;
    //     $book->author = $request->author;
    //     $book->price = $request->price;
    //     $book->describe = $request->describe;
    //     $book->type = $request->type;

    //     if(!$book->save()){
    //         return redirect()->route('cart.edit.page', $request->id);
    //     }

    //     return redirect()->route('buy');

    // }

    public function delete($id)
    {
        # code...
        // dd(1);
        $cart = Cart::find($id); //ค้นหาข้อมูลจาก id ถ้าเจอข้อมูล จะลบออก
        $cart->delete(); //ลบข้อมูลออก
        return redirect()->route('buy');
    }



}
