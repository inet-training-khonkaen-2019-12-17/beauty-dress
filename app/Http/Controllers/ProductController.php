<?php

namespace App\Http\Controllers;

use App\Product;
use App\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input as Input;
class ProductController extends Controller
{
    //
    public function index()
    {
        //dd(['a', 'b']); // โชว์ข้อมูลเป็น array
        // return view('book.index');
        $products = Product::all();
        // dd($books);
        // dd($products);
        return view('welcome',['products'=> $products]);
    }
    public function adminPage(){
        $products = Product::all();
        return view('admin.admin',['products'=>$products]);
    }

    public function buy() {

        return view('product.buy');
    }
    public function createPage()
    {
        return view('admin.create');
    }

    public function create(Request $request)
    {
        // dump($request->name);
        // dd($request->all());
        $product = new Product();
        $product->name = $request->name;
        $product->price = $request->price;
        $product->detail = $request->detail;
        $product->image_pro = $request->image_pro;
        return redirect()->route('admin.page');

    }

    public function editPage($id)
    {
        // dd($id);
        $product = Product::find($id);
        return view('admin.edit', compact('product'));
    }

    public function edit(Request $request)
    {
        // dd($request->all());
        $product = Product::find($request->id);
        $product->name = $request->name;
        $product->price = $request->price;
        $product->detail = $request->detail;
        $product->image_pro = $request->image_pro;

        return redirect()->route('admin.page');

    }

    public function delete($id)
    {
        # code...
        // dd(1);
        $product = Product::find($id); //ค้นหาข้อมูลจาก id ถ้าเจอข้อมูล จะลบออก
        $product->delete(); //ลบข้อมูลออก
        return redirect()->route('admin.page');
    }

    // public function buy() {
    //     // return view('product.buy');
    //     $products = Product::all();
    //     return view('product.buy',['products'=>$products]);
    // }

    public function editNumPage($id)
    {
        $cart = Cart::find($id);
        return view('product.edit', compact('cart'));
    }

    public function editNum(Request $request)
    {
        $cart = Cart::find($request->id);
        // dump($book);
        $cart->name = $request->name;
        $cart->price = $request->price;
        $cart->amount = $request->amount;
        $cart->total = $request->total;

        if(!$cart->save()){
                    return redirect()->route('product.edit.page', $request->id);
                }

                return redirect()->route('buy');
    }

    public function invoice() {
        $carts = Cart::all();
        return view('product.invoice',['carts'=>$carts]);
    }

    // public function createPage()
    // {
    //     return view('admin.create');
    // }
}
