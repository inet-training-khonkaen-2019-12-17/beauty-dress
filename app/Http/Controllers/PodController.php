<?php

namespace App\Http\Controllers;
use App\Pod;
use Illuminate\Http\Request;

class PodController extends Controller
{
    public function create(Request $request)
    {
        // dump($request->name);
        // dd($request->all());
        $pod = new Pod();
        $pod->firstname = $request->firstname;
        $pod->lastname = $request->lastname;
        $pod->address = $request->address;
        $pod->product_id = $request->product_id;
        $pod->amount= $request->amount;
        $pod->total= $request->total;
        $pod->status= $request->status;
        $pod->image_slip= $request->image_slip;
        return redirect()->route('admin.page');
    }
}
