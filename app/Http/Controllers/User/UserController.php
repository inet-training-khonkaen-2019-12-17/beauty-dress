<?php
namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UserRegisterRequest;
use App\Onechat;
use Illuminate\Support\Facades\Hash;
use App\User;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;




class UserController extends Controller
{
    public function registerPage(){
        return view('auth.register');
    }
    public function register(Request $request){
        $userExist = User::where('email', $request->email)->exists();

        if ($userExist) {
            return redirect()->back()->with(['status' => 'user is already existing!']);
        }
        $user = new User();

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);

        if (!$user->save()) {
            return redirect()->back();
        }

        return redirect()->route('login.page');
        // dd($userExist);
        // $validateData = $request->validate([
        //     'name' => 'required|max:255',
        //     'email' => 'required|email|max:255',
        //     'password' => 'required|min:6|confirmed',
        // ]);
        // dd($validateData);
    }

    public function loginPage()
    {
        return view('auth.login');
    }

    public function login (Request $request)
    {
        //1
        // $user = User::where('email', $request->email)
        // ->get()->first();
            // dd($user);
        // if(Hash::check($request->password, $user->password)){
            //1.1
            // Auth::login($user);
            //1.2
            // Auth::loginUsingId($user->id);
            // return redirect('/');
        // }
        $isAuth = Auth::attempt([
            'email' => $request->email,
            'password' => $request->password,
        ]);
        if (!$isAuth) {
            return redirect()->back()->with(['status' => 'Login failed!']);

        }
        return redirect('/');

    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

    public function profile()
    {
        // dd(1);
        $user = Auth::user();
        // dd($user);
        return view('auth.profile', compact('user'));
    }

    public function storeOneChat(Request $request)
    {
        # code...

        $info = $this->checkOneChatUser($request->one_mail);
        if ($info->status === 'fail') {
            return redirect()->back()->with(['status' => 'Fail!']);
        }
        $onechat = new Onechat();
        $onechat->one_mail = $request->one_mail;
        $onechat->onechat_id = $info->friend->user_id;
        $onechat->user_id = Auth::user()->id;
        if (!$onechat->save()) {
            return redirect()->back()->with(['status' => 'Save Fail!']);
        }
        $this->sendMessage('Hello I am kwangly shop',$info->friend->user_id);
        return redirect()->back();
    }
private function checkOneChatUser($email)
    {
        try {
            $client = new Client();
            $res = $client->request('POST', "https://chat-manage.one.th:8997/api/v1/searchfriend", [
                "headers" => [
                    'Authorization' => "Bearer Aff4145e77f815a1eade37e6e8ef182a7911f657dd8c1435b8504f0940c7980d525af4bc2f7174c08ba582cb2ef7e7bd8",
                    "Content-Type" => "application/json",
                ],
                'json' => [
                    'bot_id' => "B05a701152345507cbec0680309f024d3",
                    "key_search" => $email
                ]
            ]);
            $resToJson = json_decode($res->getBody()->getContents());
            return $resToJson;
        } catch (GuzzleException $e) {
           return (object) ['status' => 'fail'];
        }
    }
    private function sendMessage($msg, $onechat_id)
    {
        try {
            $client = new Client();
            $res = $client->request('POST', "https://chat-public.one.th:8034/api/v1/push_message", [
                "headers" => [
                    'Authorization' => "Bearer Aff4145e77f815a1eade37e6e8ef182a7911f657dd8c1435b8504f0940c7980d525af4bc2f7174c08ba582cb2ef7e7bd8",
                    "Content-Type" => "application/json",
                ],
                'json' => [
                    'to' => $onechat_id,
                    'bot_id' => "B05a701152345507cbec0680309f024d3",
                    'type' => 'text',
                    "message" => $msg,
                ]
            ]);
            return null;
        } catch (GuzzleException $e) {
            return null;
        }
    }
}
